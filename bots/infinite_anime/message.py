import importlib
import logging
import os
import re
import time

from aiohttp import web
from aiojobs.aiohttp import spawn

from . import vkapi
from .command_proc import commands_list
from .config import conf_token, secret

bot_path = os.path.split(os.path.relpath(__file__))[0]


def distance_levenshtein(s1, s2):
    length1, length2 = len(s1), len(s2)
    if length1 > length2:
        s1, s2 = s2, s1
        length1, length2 = length2, length1
    current_row = range(length1 + 1)
    for i in range(1, length2 + 1):
        previous_row, current_row = current_row, [i] + [0] * length1
        for j in range(1, length1 + 1):
            add, delete, change = previous_row[j] + 1, current_row[j - 1] + 1, previous_row[j - 1]
            if s1[j - 1] != s2[i - 1]:
                change += 1
            current_row[j] = min(add, delete, change)
    return current_row[length1]


def load_modules():
    modules_path = os.path.join(bot_path, "commands")
    files = os.listdir(modules_path)
    modules = filter(lambda x: x.endswith('.py'), files)
    for m in modules:
        importlib.import_module(modules_path.replace("\\", ".").replace("/", ".") + '.' + m[0:-3])


async def find_command(user_id, data, redis):
    message = data['object']['text'].lower()
    message = re.sub(r'\[[^\]]*\]', '', message).strip()
    logging.debug(r"[Anime: {}]".format(message.replace('\n', '\\n')))
    len_message = len(message)
    if len_message < 30:
        for c in commands_list:
            if (c.settings == 'user' or c.settings == 'user/conv') and data['object']['peer_id'] < 2_000_000_000:
                for k in c.keys:
                    d = distance_levenshtein(message, k)
                    if len_message > d:
                        if d == 0 or len_message * 0.4 > d:
                            reply, attachment = await c.process(user_id, data, redis)
                            return reply, attachment
            elif (c.settings == 'conv' or c.settings == 'user/conv') and data['object']['peer_id'] > 2_000_000_000:
                for k in c.keys:
                    d = distance_levenshtein(message, k)
                    if len_message > d:
                        if d == 0 or len_message * 0.4 > d:
                            reply, attachment = await c.process(user_id, data, redis)
                            return reply, attachment
        return 'Я не понял вашу команду.\nУзнать список команд можно написав "Помощь"', ''
    else:
        return '', ''


async def use_command(user_id, data, redis):
    message = data['object']['text'].lower()
    message = re.sub(r'\[[^\]]*\]', '', message).strip()
    start_time = time.time()
    attachment = ''
    # keyboard = False
    if message == 'начать':
        reply = 'Привет\n\nПиши "помощь" чтобы узнать список команд\n\nАктивно ' \
                'развиваюсь, пиши свои предложения в специальном' \
                ' обсуждении\n\nhttps://vk.com/topic-159945083_36828919'
    else:
        if 'payload' in data['object']:
            from .commands.recommendation_anime import get_anime
            reply, attachment = await get_anime(data['object'], redis)
        else:
            reply, attachment = await find_command(user_id, data, redis)

    if reply or attachment:
        logging.debug("Command used. ResponseTime: {}".format(time.time() - start_time))
        logging.debug("reply: %s", reply)
        logging.debug("attachment: %s", attachment)
        if user_id == data['object']['from_id']:
            await vkapi.send_message(user_id, reply, attachment, keyboard=True)
        else:
            if reply != 'Я не понял вашу команду.\nУзнать список команд можно написав "Помощь"':
                await vkapi.send_message(user_id, reply, attachment)
            else:
                await vkapi.mark_as_read(data['object']['peer_id'], True)
        logging.debug("ResponseTime: {}".format(time.time() - start_time))
    else:
        await vkapi.mark_as_read(data['object']['peer_id'], True)


load_modules()


async def msg_processor(request):
    data = await request.json()
    if 'type' not in data.keys():
        return web.Response(body='Not VK =(', status=405)
    if data.get('secret', '') == secret:
        if data['type'] == 'confirmation':
            return web.Response(body=conf_token, status=200)
        elif data['type'] == 'message_new':
            if data['object']['peer_id'] > 2000000000:
                from_id = data['object'].get("from_id")
            else:
                from_id = data['object'].get('peer_id')
            redis = request.app['redis_1']
            check_ttl_id = await redis.get(f"INFmain:{from_id}")
            if not check_ttl_id:
                await redis.setex(f"INFmain:{from_id}", 3, 1)
                await spawn(request, use_command(data['object']['peer_id'], data, redis))
            elif 0 < int(check_ttl_id) < 8:
                if 4 < int(check_ttl_id) < 8:
                    await vkapi.send_message(data['object'].get('peer_id'),
                                             f'@id{from_id} (Ты) пишешь слишком часто!({int(check_ttl_id) - 2})', '')
                await redis.setex(f"INFmain:{from_id}", int(check_ttl_id) * 2, int(check_ttl_id) + 1)
                await spawn(request, use_command(data['object']['peer_id'], data, redis))
            elif 8 < int(check_ttl_id):
                await redis.setex(f"INFmain:{from_id}", 600, -1)
                await vkapi.send_message(data['object'].get('peer_id'),
                                         f'Ну @id{from_id} (ты) конечно молодец))\nЯ забанил тебя на 10 минут!', '')
            else:
                await vkapi.mark_as_read(data['object']['peer_id'], True)
        return web.Response(body='ok', status=200)
    else:
        return web.Response(body='error', status=405)
