import datetime
import logging

import aiohttp
from bs4 import BeautifulSoup

from .. import command_proc
from .. import vkapi
from ..shikimoriapi import ShikimoriApi
from ..tracemoe import TraceMoeAPI


async def search(user_id, msg, redis):
    check_ttl_id = await redis.get(f"what_anime:{user_id}")
    if check_ttl_id:
        ttl = await redis.ttl(f"what_anime:{user_id}")
        return f'Повтори эту команду через {ttl} сек', ''
    temp = msg['object']
    img_url = {}
    not_attach_and_not_fwd_msgs = not temp['attachments'] and not temp['fwd_messages']
    not_attach_and_fwd_msgs_attach = not temp['attachments'] and temp['fwd_messages'] and not \
        temp['fwd_messages'][0]['attachments']
    if not_attach_and_not_fwd_msgs or not_attach_and_fwd_msgs_attach:
        return 'Напиши "откуда" и прикрепи скриншот, гифку или видео к сообщению\n' \
               'Обрати внимание, я работаю ТОЛЬКО СО СКРИНШОТАМИ!\n' \
               'В случае с гифкой и видео, в гифке или видео должен быть момент из аниме', ''
    elif not temp['attachments'] and temp['fwd_messages']:
        if temp['fwd_messages'][0]['attachments']:
            temp = temp['fwd_messages'][0]
    try:
        if temp['attachments'][0]['type'] == 'wall' and temp['attachments'][0]['wall']['attachments']:
            temp = temp['attachments'][0]['wall']
        if temp['attachments'][0]['type'] == 'photo':
            for size in temp['attachments'][0]['photo']['sizes']:
                size_condition = size['type'] == 'z' or size['type'] == 'y' or size['type'] == 'x' or size[
                    'type'] == 'r'
                if size_condition:
                    img_url[size['type']] = size['url']
        elif temp['attachments'][0]['type'] == 'doc' and temp['attachments'][0]['doc']['ext'] == 'gif':
            for size in temp['attachments'][0]['doc']['preview']['photo']['sizes']:
                size_condition = size['type'] == 'z' or size['type'] == 'y' or size['type'] == 'x' or size[
                    'type'] == 'r' or size['type'] == 'o'
                if size_condition:
                    img_url[size['type']] = size['src']
        elif temp['attachments'][0]['type'] == 'video':

            format_mapping = {
                "photo_1280": 'z',
                "photo_800": 'y',
                "first_frame_720": 'x',
                "photo_640": 'r',
                "photo_320": 'o'
            }

            video = temp['attachments'][0]['video']
            for photo_size, img_type in format_mapping.items():
                if photo_size in video:
                    img_url[img_type] = video[photo_size]

        if 'y' in img_url:
            image_url = img_url.get('y')
        elif 'z' in img_url:
            image_url = img_url.get('z')
        elif 'x' in img_url:
            image_url = img_url.get('x')
        elif 'r' in img_url:
            image_url = img_url.get('r')
        elif 'o' in img_url:
            image_url = img_url.get('o')
        else:
            return 'Ошибка. Попробуй другое изображение, гифку или видео', ''
        async with aiohttp.request('GET', image_url) as resp:
            image = await resp.read()
    except Exception as e:
        logging.exception(e)
        return f'Ошибка. Попробуй другое изображение, гифку или видео!', ''
    tracemoe = TraceMoeAPI()
    await vkapi.send_message(user_id, f"Начинаю поиск!\n\n"
                                      f"В связи с новыми ограничениями, поиск может затянуться, "
                                      f"если ждать не хочется, результат можно получить быстрее по ссылке:\n"
                                      f"{tracemoe.get_search_on_site_url(image_url)}")
    await redis.setex(f"what_anime:{user_id}", 60, 'True')

    try:
        response = await tracemoe.search(image)
        response = response.get('result')
        if not response:
            return f'К сожалению поиск не удался, возможно бот превысил квоту\n' \
                   f'Поиск на сайте: {tracemoe.get_search_on_site_url(image_url)}'
    except Exception as e:
        logging.exception(e)
        await redis.delete(f"what_anime:{user_id}")
        return f'Я не смог найти откуда этот скриншот\nВозможно это внутренняя ошибка' \
               f'\nПопробуй поискать на сайте: {tracemoe.get_search_on_site_url(image_url)}', ''
    if response[0]['similarity'] >= 0.8 and response[0].get('error') is None:
        try:
            anilist = await tracemoe.get_data_from_anilist(response[0].get('anilist'))
            is_adult = anilist.get('data').get('Media').get('isAdult')
            en_title = anilist.get('data').get('Media').get('title').get('romaji')
            mal_id = anilist.get('data').get('Media').get('idMal')
        except Exception as e:
            logging.exception(e)
            en_title = response[0].get('filename')
            mal_id = None
            is_adult = True
        anime_info = {'russian': ''}
        message = ''
        try:
            if mal_id:
                shikiapi = ShikimoriApi()
                await shikiapi.create_headers()
                anime_info = await shikiapi.get_info_anime(mal_id)
                anime_info['russian'] = f"{anime_info['russian']} / "
                genres = ', '.join([x['russian'] for x in anime_info['genres']])
                soup = BeautifulSoup(
                    anime_info['description_html'].replace('<div class="b-prgrph">', '\n').replace('<br>', '\n'),
                    'lxml')
                try:
                    # description = '\n'.join([x.text for x in soup.find_all(class_='b-prgrph')])
                    description = soup.get_text()
                except:
                    description = ''
                # description = re.sub(r'\[.*?\]', '', anime_info['description'].replace('[[', '').replace(']]', ''))
                message = f"Жанры: {genres}\n" \
                          f"Рейтинг: {anime_info['score']}\n" \
                          f"\n{description}\n\n" \
                          f"Подробнее на shikimori: https://shikimori.one{anime_info['url']}" \
                          f"\nПодробнее на MyAnimeList: https://myanimelist.net/anime/{mal_id}\n"
        except:
            logging.exception('error')
        if response[0]['episode'] == 0 or not response[0]['episode'] or response[0]['episode'] == '':
            message = f"Сходство {round(response[0]['similarity'] * 100, 1)}%\n" \
                      f"Аниме: {anime_info['russian']}{en_title}\n" \
                      f"Время в эпизоде: {str(datetime.timedelta(seconds=round(response[0]['from'])))}\n{message}"
        else:
            message = f"Сходство {round(response[0]['similarity'] * 100, 1)}%\n" \
                      f"Аниме: {anime_info['russian']}{en_title}\n" \
                      f"Эпизод: {response[0]['episode']}\n" \
                      f"Время в эпизоде: {str(datetime.timedelta(seconds=round(response[0]['from'])))}\n{message}"
        attachment = ''
        try:
            if not is_adult:
                async with aiohttp.ClientSession() as session:
                    async with session.get(response[0]["image"]) as resp:
                        thumbnail = await resp.read()
                image_uploader = vkapi.ImageUploader(user_id, thumbnail)
                attachment = await image_uploader.upload()
                message += "\n└(＾＾)┐"
        except Exception as e:
            logging.exception(e)
        return message, attachment
    else:
        await redis.delete(f"what_anime:{user_id}")
        return 'Я не смог найти откуда этот скриншот\nБыть может изображение ' \
               'слишком темное, либо это вовсе не скриншот\n' \
               'Или этот скриншот не из аниме\n' \
               'Если это гифка или видео, то там должен быть момент из аниме', ''


command = command_proc.Command()

command.keys = ['откуда', 'откуда скриншот']
command.description = 'По скриншоту, гифке или видео постараюсь найти аниме'
command.settings = 'user/conv'
command.process = search
